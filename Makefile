CXX       = c++
CXXFLAGS  = -ansi
CXXFLAGS += -Wall -Wextra -Wconversion -pedantic
#CXXFLAGS += -pg
#CXXFLAGS += -DDEBUG -ggdb3
CXXLIBS   = -lm

all: build

build: knapsack

knapsack: knapsack.cc
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o knapsack knapsack.cc

clean:
	rm -f knapsack
