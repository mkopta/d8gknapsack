/* Copyright (c) <2009> <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*****************************************************************/

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <assert.h>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <queue>

using std::vector;
using std::string;
using std::ifstream;
using std::sort;
using std::priority_queue;

class Item {
  private:
    int price, weight, id;
    double ratio; /* price to weight */
    bool taken;  /* goes into knapsack or not */
  public:
    Item(const int _price, const int _weight, const int _id)
      :price(_price), weight(_weight), id(_id), taken(false) {
      this->ratio = static_cast<double>(_price) / static_cast<double>(_weight);
    }

    Item(const Item &i) {
      price = i.price;
      weight = i.weight;
      id = i.id;
      ratio = i.ratio;
      taken = i.taken;
    }

    int get_price() const {
      return this->price;
    }

    int get_weight() const {
      return this->weight;
    }

    double get_ratio() const {
      return this->ratio;
    }

    int get_id() const {
      return this->id;
    }

    void take(const bool _bool) {
      this->taken = _bool;
    };

    bool is_taken() const {
      return this->taken;
    };

    bool operator==(const Item &i2) const {
      return i2.id == this->id;
    }

    bool operator!=(const Item &i2) const {
      return i2.id != this->id;
    }
};

class Knapsack {
  private:
    int max_load, value, load;
  public:
    explicit Knapsack(const int _max_load)
      :max_load(_max_load), value(0), load(0)
    {}

    int get_load() const {
      return this->load;
    }

    int get_value() const {
      return this->value;
    }

    int get_max_load() const {
      return this->max_load;
    }

    bool try_add(const Item item) {
      if ((this->load + item.get_weight()) > this->max_load) {
        return false;
      } else {
        this->load += item.get_weight();
        this->value += item.get_price();
        return true;
      }
    }

    void clear() {
      this->value = 0;
      this->load = 0;
    }
};

class Instance {
  private:
    int id, max_load;
    unsigned items_count;
    unsigned long int number_of_tested_states;
    vector<Item> items;
  public:
    Instance(int _id, int _items_count, int _max_load,
        vector<Item> _items)
      :id(_id), max_load(_max_load), items_count(_items_count),
      number_of_tested_states(0), items(_items)
    {}

    int get_id() const {
      return this->id;
    }

    unsigned get_items_count() const {
      return this->items_count;
    }

    int get_max_load() const {
      return this->max_load;
    }

    Item get_item(const int position) const {
      assert(static_cast<unsigned>(position) < this->items.size());
      return this->items[static_cast<unsigned>(position)];
    }

    vector<Item> get_items() const {
      return this->items;
    }

    void set_items(vector<Item> _items) {
      this->items = _items;
    }

    unsigned long int get_number_of_tested_states() const {
      return this->number_of_tested_states;
    }

    void set_number_of_tested_states(unsigned long int count) {
      this->number_of_tested_states = count;
    }

    int get_sum_price_of_all_items() const {
      int sum_price = 0;
      for (unsigned i = 0; i < this->items.size(); i++)
        sum_price += this->items.at(i).get_price();
      return sum_price;
    }
};

class IOHandler {
  private:
    vector<Instance> instances;
    vector<string> explode(string pattern, string text) {
      vector<string> tokens;
      int mark = 0;
      size_t found;
      string token;

      if (pattern.empty()) {
        tokens.push_back(text);
        return tokens;
      } else if (text.empty()) {
        return tokens;
      }

      while (true) {
        found = text.find(pattern, static_cast<size_t>(mark));
        if (found != string::npos) {
          if (static_cast<int>(found) - mark) {
            token = text.substr(mark, static_cast<int>(found) - mark);
            tokens.push_back(token);
          }
          mark = static_cast<int>(found) + static_cast<int>(pattern.length());
        } else {
          if (mark - text.length()) {
            token = text.substr(mark, text.length());
            tokens.push_back(token);
          }
          break;
        }
      }
      return tokens;
    }
  public:
    int load_data(const char *const filename) {
      string line;
      ifstream data(filename);
      if (data.is_open()) {
        while (!data.eof()) {
          getline(data, line);
          vector<string> strings = explode(string(" "), line);
          vector<int> numbers;
          for (unsigned i = 0; i < strings.size(); i++)
            numbers.push_back(atoi(strings[i].c_str()));
          if (numbers.empty()) {
            /* skip empty lines */
            continue;
          } else if (numbers.size() < 3U) {
            fprintf(stderr, "Error while loading data from '%s'", filename);
            fprintf(stderr, ", line must contain at least ID, items count");
            fprintf(stderr, " and max load\n");
            return 1;
          } else if (numbers[1] != (static_cast<int>(numbers.size()) - 3) / 2) {
            fprintf(stderr, "Error while loading data from '%s'", filename);
            fprintf(stderr, ", unmatched number of items on line\n");
            return 1;
          } else {
            /* everything ok, creating new instance */
            vector<Item> items;
            for (unsigned i = 3; i < numbers.size() - 1; i+=2) {
              items.push_back(Item(numbers[i+1], numbers[i], i-3));
            }
            Instance inst(numbers[0], numbers[1], numbers[2], items);
            this->instances.push_back(inst);
          }
        }
        data.close();
      } else {
        fprintf(stderr, "Error while loading data from '%s'\n", filename);
        return 1;
      }
      return 0;
    }

    int instances_count() {
      return static_cast<int>(this->instances.size());
    }

    Instance get_instance(int position) {
      assert(static_cast<unsigned>(position) < this->instances.size());
      return this->instances[static_cast<unsigned>(position)];
    }
};

namespace Algorithms {

namespace Branch_and_Bound_ns {
  enum item_state {TAKEN, NOTTAKEN, UNDEF};

  class Offer {
    private:
      int price;
      int weight;
      int max_price;
      vector<item_state> configuration;
      bool final;
    public:
      Offer(int _price, int _weight, int _max_price,
          vector<item_state> _configuration, bool _final)
        :price(_price), weight(_weight), max_price(_max_price),
        configuration(_configuration), final(_final) {}

      int get_price() const {
        return this->price;
      }

      int get_weight() const {
        return this->weight;
      }

      int get_max_price() const {
        return this->max_price;
      }

      vector<item_state> get_configuration() const {
        return this->configuration;
      }

      bool is_final() const {
        return this->final;
      }

      void set_is_final(bool _is) {
        this->final = _is;
      }

      unsigned get_index_of_first_undef() const {
        for (unsigned i = 0; i < configuration.size(); i++)
          if (configuration[i] == UNDEF)
            return i;
        return static_cast<unsigned>(configuration.size());
      }

      void set_item(const unsigned index, const item_state state) {
        configuration[index] = state;
      }

      void set_price(const int _price) {
        this->price = _price;
      }

      void set_weight(const int _weight) {
        this->weight = _weight;
      }

      void set_max_price(const int _max_price) {
        this->max_price = _max_price;
      }

      bool operator<(const Offer &o) const {
        if (o.max_price > this->max_price) {
          return true;
        } else {
          return false;
        }
      }
  };
}

class Branch_and_Bound {
  private:
    Knapsack knapsack;
    Instance instance;

    vector<Branch_and_Bound_ns::Offer>
      expand(Branch_and_Bound_ns::Offer offer) {
      using namespace Branch_and_Bound_ns;
      vector<Item> items = this->instance.get_items();
      vector<Offer> new_offers;
      Offer new1(offer), new2(offer);
      unsigned i = offer.get_index_of_first_undef();
      assert(i != items.size());
      new1.set_item(i, TAKEN);
      new1.set_price(offer.get_price() + items[i].get_price());
      new1.set_weight(offer.get_weight() + items[i].get_weight());
      new2.set_item(i, NOTTAKEN);
      new2.set_max_price(offer.get_max_price() - items[i].get_price());
      if (new1.get_index_of_first_undef() == items.size()) {
        new1.set_is_final(true);
        new2.set_is_final(true);
      }
      new_offers.push_back(new1);
      new_offers.push_back(new2);
      return new_offers;
    }

  public:
    Branch_and_Bound(const Knapsack _knapsack, const Instance _instance)
      :knapsack(_knapsack), instance(_instance)
    {}

    void go() {
      using namespace Branch_and_Bound_ns;
      unsigned long int number_of_tested_states = 0;
      priority_queue<Offer> offers;
      vector<Item> items = instance.get_items();
      vector<item_state> configuration;
      int max_price = 0;
      for (unsigned int i = 0; i < items.size(); i++) {
        max_price += items[i].get_price();
        configuration.push_back(UNDEF);
      }
      Offer empty_offer(0, 0, max_price, configuration, false);
      Offer best_offer = empty_offer;
      offers.push(empty_offer);
      while (not offers.empty()) {
        Offer offer = offers.top();
        offers.pop();
        vector<Offer> new_offers = expand(offer);
        for (unsigned int i = 0; i < new_offers.size(); i++) {
          if (new_offers[i].get_weight() <= this->instance.get_max_load()) {
            if (new_offers[i].is_final()) {
              if (new_offers[i].get_price() > best_offer.get_price()) {
                best_offer = new_offers[i];
                number_of_tested_states++;
              }
            } else {
              offers.push(new_offers[i]);
            }
          } else {
            number_of_tested_states++;
          }
        }
        while (not offers.empty() &&
            offers.top().get_max_price() <= best_offer.get_price()) {
          offers.pop();
          number_of_tested_states++;
        }
      }

      this->knapsack.clear();
      vector<item_state> best_conf = best_offer.get_configuration();
      for (unsigned i = 0; i < best_conf.size(); i++) {
        if (best_conf[i] == TAKEN) {
          items[i].take(true);
          this->knapsack.try_add(items[i]);
        } else {
          items[i].take(false);
        }
      }
      this->instance.set_items(items);
      this->instance.set_number_of_tested_states(number_of_tested_states);
    }

    Knapsack get_knapsack() const {
      return this->knapsack;
    }

    Instance get_instance() const {
      return this->instance;
    }
};

namespace Dynamic_programming_ns {

#include <limits.h>

/* inf + something small = inf (thats why 'minus something bigger'..) */
const unsigned inf = INT_MAX - (INT_MAX / 3);

class Table {
  private:
    int *table;
    unsigned rows, cols;

  public:

    Table(const unsigned _rows, const unsigned _cols):
      rows(_rows), cols(_cols) {
      table = new int[_rows *  _cols];
    }

    ~Table() {
      delete table;
    }

    /* & = rw */
    int &operator()(const unsigned row, const unsigned col) {
      return table[row * this->cols + col];
    }

    unsigned get_rows() const {
      return this->rows;
    }

    unsigned get_cols() const {
      return this->cols;
    }

#ifdef DEBUG
    void dump() const {
      putchar('\n');
      for (unsigned i = 0; i < this->rows; i++) {
        printf("price: %u | ", i);
        for (unsigned j = 0; j < this->cols; j++) {
          printf("%4d ", (this->table)[i * this->cols + j]);
        }
        printf("|\n");
      }
      putchar('\n');
    }
#endif
};

}

class Dynamic_programming {
  private:
    Knapsack knapsack;
    Instance instance;

  public:
    Dynamic_programming(const Knapsack _knapsack, const Instance _instance)
      :knapsack(_knapsack), instance(_instance)
    {}

    void go() {
      using Dynamic_programming_ns::Table;
      using Dynamic_programming_ns::inf;
      unsigned long int number_of_tested_states = 0;

      unsigned rows = this->instance.get_sum_price_of_all_items() + 1;
      unsigned cols = this->instance.get_items_count() + 1;

      Table t(rows, cols);
      Table u(rows, cols);

      t(0, 0) = 0;
      for (unsigned i = 1; i < rows; i++) {
        t(i, 0) = inf;
      }

      for (unsigned i = 0; i < rows; i++) {
        for (unsigned j = 0; j < cols; j++) {
          u(i, j) = inf;
        }
      }

      for (unsigned j = 1; j < cols; j++) {
        for (unsigned i = 0; i < rows; i++) {
          int weight = t(i, j - 1);
          int thing = u(i, j - 1);
          if (i >=
              static_cast<unsigned>(this->instance.get_item(j - 1).get_price()))
          {
            int weight2 = t(i - this->instance.get_item(j - 1).get_price(),
                j - 1) + this->instance.get_item(j - 1).get_weight();
            if (weight > weight2) {
              weight = weight2;
              thing = j - 1;
            }
          }
          number_of_tested_states++;
          t(i, j) = weight;
          u(i, j) = thing;
        }
      }

#ifdef DEBUG
      //t.dump();
#endif

      bool found = false;
      unsigned i = rows;
      unsigned j = cols;
      while (!found) {
        j--;
        while (!found)
          if (t(--i, j) <= this->knapsack.get_max_load())
            found = true;
      }

      vector<Item> items = this->instance.get_items();
      for (unsigned r = 0; r < items.size(); r++)
        items[r].take(false);
      items[u(i, j)].take(true);
      while (--j > 0) {
        if (u(i, j) != u(i, j + 1)) {
          i -= items[u(i, j + 1)].get_price();
          if (i == 0) break;
          items[u(i, j)].take(true);
        }
      }
      this->instance.set_items(items);
      this->instance.set_number_of_tested_states(number_of_tested_states);
      fill_knapsack();
    }

    void fill_knapsack() {
      for (unsigned i = 0; i < this->instance.get_items_count(); i++) {
        if (this->instance.get_item(i).is_taken()) {
          this->knapsack.try_add(this->instance.get_item(i));
        }
      }
    }

    Knapsack get_knapsack() const {
      return this->knapsack;
    }

    Instance get_instance() const {
      return this->instance;
    }
};

class Simulated_annealing {
  private:
    Knapsack knapsack;
    Instance instance;
    double initial_temperature;

    vector<Item> nothing_taken() const {
      vector<Item> state(this->instance.get_items());
      for (unsigned i = 0; i < state.size(); i++)
        state[i].take(false);
      return state;
    }

    /* Returns number in range <0, 1> */
    double random() const {
      return rand() / static_cast<double>(RAND_MAX);
    }

    unsigned random_int(const unsigned min, const unsigned max) const {
      return (rand() % (max - min + 1)) + min;
    }

    /* Calculated value of given state */
    int value(const vector<Item> state) const {
      int v = 0;
      for (unsigned i = 0; i < state.size(); i++)
        if (state[i].is_taken())
          v += state[i].get_price();
      return v;
    }

    int weight(const vector<Item> state) const {
      int w = 0;
      for (unsigned i = 0; i < state.size(); i++)
        if (state[i].is_taken())
          w += state[i].get_weight();
      return w;
    }

    double cool(const double temperature, const double cooling_factor) const {
      return temperature * cooling_factor;
    }

    vector<Item> pick_random_neighbor(const vector<Item> state) const {
      int max_load = this->knapsack.get_max_load();
      vector<Item> neighbor = state;
      unsigned item_number;
      unsigned items_count = static_cast<unsigned>(neighbor.size());

      while (true) {
        item_number = random_int(0U, items_count - 1);
        neighbor[item_number].take(not neighbor[item_number].is_taken());
        if (weight(neighbor) <= max_load) {
          return neighbor;
        } else {
          // Not usable configuration, trying another one
          neighbor[item_number].take(not neighbor[item_number].is_taken());
        }
      }
    }

  public:
    Simulated_annealing(const Knapsack _knapsack, const Instance _instance)
      :knapsack(_knapsack), instance(_instance) {
    }

    void go() {
      vector<Item> s = nothing_taken(); /* initial state */
      vector<Item> s_new;               /* newly picked state */
      double t = 30.0;                  /* initial temperature */
      double t_min = 0.1;               /* minimal temperature */
      double alpha = 0.999;             /* cooling factor */
      int value_now = value(s);         /* cached value of current state */
      int value_new;                    /* cached value of new state */
      unsigned long int states_count = 0;


      while (t > t_min) {
        s_new = pick_random_neighbor(s);
        value_new = value(s_new);
        if (value_new > value_now) {
          s = s_new;
          value_now = value_new;
        } else {
          // value_new < value_now || value_new == value_now
          int delta = value_new - value_now;
          if (random() < exp(static_cast<double>(delta)/t)) {
            // take
            s = s_new;
            value_now = value_new;
          }
        }
        t = cool(t, alpha);
        states_count++;
      }

      this->instance.set_items(s);
      this->instance.set_number_of_tested_states(states_count);
      for (unsigned i = 0; i < s.size(); i++) {
        if (s[i].is_taken())
          this->knapsack.try_add(s[i]);
      }
    }

    Knapsack get_knapsack() const {
      return this->knapsack;
    }

    Instance get_instance() const {
      return this->instance;
    }
};

}

class Solver {
  private:
    Knapsack knapsack;
    Instance instance;
    int selected_strategy;

    static bool sort_by_ratio_DA(const Item& a, const Item& b) {
      if (a.get_ratio() == b.get_ratio())
        return a.get_ratio() < b.get_ratio();
      return a.get_ratio() > b.get_ratio();
    }

    static bool sort_by_ratio_UA(const Item& a, const Item& b) {
      if (a.get_ratio() == b.get_ratio())
        return a.get_ratio() > b.get_ratio();
      return a.get_ratio() < b.get_ratio();
    }

    static bool sort_by_price_DA(const Item& a, const Item& b) {
      if (a.get_price() == b.get_price())
        return a.get_price() < b.get_price();
      return a.get_price() > b.get_price();
    }

    static bool sort_by_price_UA(const Item& a, const Item& b) {
      if (a.get_price() == b.get_price())
        return a.get_price() > b.get_price();
      return a.get_price() < b.get_price();
    }

    static bool sort_by_weight_DA(const Item& a, const Item& b) {
      if (a.get_weight() == b.get_weight())
        return a.get_weight() < b.get_weight();
      return a.get_weight() > b.get_weight();
    }

    static bool sort_by_weight_UA(const Item& a, const Item& b) {
      if (a.get_weight() == b.get_weight())
        return a.get_weight() > b.get_weight();
      return a.get_weight() < b.get_weight();
    }

    /* upwardly */
    static bool sort_by_id(const Item& a, const Item& b) {
      if (a.get_id() == b.get_id())
        return a.get_id() > b.get_id();
      return a.get_id() < b.get_id();
    }

    vector<bool> int2bin(long int number, unsigned size) {
      vector<bool> vec;
      for (int i = size - 1; i >= 0; i--) {
        if ((pow(2, i) <= number)) {
          number -= static_cast<int>(pow(2, i));
          vec.push_back(true);
        } else {
          vec.push_back(false);
        }
      }
      return vec;
    }

    void solve_using_bruteforce() {
      vector<Item> items = this->instance.get_items();
      int best_value = 0;
      unsigned long int number_of_tested_states = 0;
      vector<bool> best_conf(items.size(), false);
      long int combinations_count =
        static_cast<int>(pow(2.0, static_cast<double>(items.size())));
      long int combination = 1;
      while (combination < combinations_count) {
        vector<bool> configuration =
          int2bin(combination, static_cast<unsigned>(items.size()));
        items = this->instance.get_items();
        assert(configuration.size() == items.size());
        for (unsigned i = 0; i < items.size(); i++)
          if (configuration[i] == true)
            if (this->knapsack.try_add(items[i]) == false)
              goto next_combination;
        if (this->knapsack.get_value() > best_value) {
          best_value = this->knapsack.get_value();
          best_conf = configuration;
        }
 next_combination:
        this->knapsack.clear();
        combination++;
        number_of_tested_states++;
      }
      this->knapsack.clear();
      items = this->instance.get_items();
      for (unsigned i = 0; i < items.size(); i++) {
        if (best_conf[i] == true) {
          this->knapsack.try_add(items[i]);
          items[i].take(true);
        } else {
          items[i].take(false);
        }
      }
      this->instance.set_items(items);
      this->instance.set_number_of_tested_states(number_of_tested_states);
    }

    void solve_using_greedy() {
      unsigned long int number_of_tested_states = 0;
      vector<Item> items = this->instance.get_items();
      /* DA = downwardly, biggest values comes first
       * UA = upwardly, lowest values comes first */
      switch (this->selected_strategy) {
        case GREEDY_RATIO_UA:
        case GREEDY_RATIO_UA_MVI:
          sort(items.begin(), items.end(), sort_by_ratio_UA);
          break;
        case GREEDY_RATIO_DA:
        case GREEDY_RATIO_DA_MVI:
          sort(items.begin(), items.end(), sort_by_ratio_DA);
          break;
        case GREEDY_PRICE_UA:
        case GREEDY_PRICE_UA_MVI:
          sort(items.begin(), items.end(), sort_by_price_UA);
          break;
        case GREEDY_PRICE_DA:
        case GREEDY_PRICE_DA_MVI:
          sort(items.begin(), items.end(), sort_by_price_DA);
          break;
        case GREEDY_WEIGHT_UA:
        case GREEDY_WEIGHT_UA_MVI:
          sort(items.begin(), items.end(), sort_by_weight_UA);
          break;
        case GREEDY_WEIGHT_DA:
        case GREEDY_WEIGHT_DA_MVI:
          sort(items.begin(), items.end(), sort_by_weight_DA);
          break;
        default:
          fprintf(stderr, "Unknown strategy. Aborting.\n");
          return;
      }
      vector<Item>::iterator item;
      Item most_valuable_item = *(items.begin());
      for (item = items.begin(); item != items.end(); item++) {
        item->take(this->knapsack.try_add(*item));
        number_of_tested_states++;
        if (item->get_price() > most_valuable_item.get_price())
          most_valuable_item = *item;
      }
      if (this->selected_strategy == GREEDY_RATIO_UA_MVI ||
          this->selected_strategy == GREEDY_RATIO_DA_MVI ||
          this->selected_strategy == GREEDY_PRICE_UA_MVI ||
          this->selected_strategy == GREEDY_PRICE_DA_MVI ||
          this->selected_strategy == GREEDY_WEIGHT_UA_MVI ||
          this->selected_strategy == GREEDY_WEIGHT_DA_MVI ) {
        if (this->knapsack.get_value() < most_valuable_item.get_price()) {
          this->knapsack.clear();
          this->knapsack.try_add(most_valuable_item);
          for (item = items.begin(); item != items.end(); item++) {
            if (*item != most_valuable_item) {
              item->take(false);
            } else {
              item->take(true);
            }
          }
        }
      }
      number_of_tested_states++;
      sort(items.begin(), items.end(), sort_by_id);
      this->instance.set_items(items);
      this->instance.set_number_of_tested_states(number_of_tested_states);
    }

    void print_availible_strategies() {
      fprintf(stderr, "Availible strategies:\n");
      fprintf(stderr, "\tbranch_and_bound\n");
      fprintf(stderr, "\tbruteforce\n");
      fprintf(stderr, "\tsimulated_annealing\n");
      fprintf(stderr, "\tdynamic_programming\n");
      fprintf(stderr, "\tgreedy_price_da\n");
      fprintf(stderr, "\tgreedy_price_da_mvi\n");
      fprintf(stderr, "\tgreedy_price_ua\n");
      fprintf(stderr, "\tgreedy_price_ua_mvi\n");
      fprintf(stderr, "\tgreedy_ratio_da\n");
      fprintf(stderr, "\tgreedy_ratio_da_mvi\n");
      fprintf(stderr, "\tgreedy_ratio_ua\n");
      fprintf(stderr, "\tgreedy_ratio_ua_mvi\n");
      fprintf(stderr, "\tgreedy_weight_ua\n");
      fprintf(stderr, "\tgreedy_weight_ua_mvi\n");
      fprintf(stderr, "\tgreedy_weight_da\n");
      fprintf(stderr, "\tgreedy_weight_da_mvi\n");
    }

  public:
    enum strategy {
      /* DA = downwardly, biggest values comes first
       * UA = upwardly, lowest values comes first */
      BRUTE_FORCE,
      GREEDY_RATIO_UA,  /* ratio = price / weight */
      GREEDY_RATIO_DA,
      GREEDY_PRICE_UA,
      GREEDY_PRICE_DA,
      GREEDY_WEIGHT_UA,
      GREEDY_WEIGHT_DA,
      GREEDY_RATIO_UA_MVI,
      GREEDY_RATIO_DA_MVI,
      GREEDY_PRICE_UA_MVI,
      GREEDY_PRICE_DA_MVI,
      GREEDY_WEIGHT_UA_MVI,
      GREEDY_WEIGHT_DA_MVI,
      BRANCH_AND_BOUND,
      DYNAMIC_PROGRAMMING,
      SIMULATED_ANNEALING
    };

    Solver(Knapsack _knapsack, Instance _instance, const char *strategy_name)
      :knapsack(_knapsack),
      instance(_instance) {
        if (!strcmp(strategy_name, "greedy_ratio_ua")) {
          this->selected_strategy = GREEDY_RATIO_UA;
        } else if (!strcmp(strategy_name, "greedy_ratio_da")) {
          this->selected_strategy = GREEDY_RATIO_DA;
        } else if (!strcmp(strategy_name, "greedy_price_ua")) {
          this->selected_strategy = GREEDY_PRICE_UA;
        } else if (!strcmp(strategy_name, "greedy_price_da")) {
          this->selected_strategy = GREEDY_PRICE_DA;
        } else if (!strcmp(strategy_name, "greedy_weight_ua")) {
          this->selected_strategy = GREEDY_WEIGHT_UA;
        } else if (!strcmp(strategy_name, "greedy_weight_da")) {
          this->selected_strategy = GREEDY_WEIGHT_DA;
        } else if (!strcmp(strategy_name, "greedy_ratio_ua_mvi")) {
          this->selected_strategy = GREEDY_RATIO_UA_MVI;
        } else if (!strcmp(strategy_name, "greedy_ratio_da_mvi")) {
          this->selected_strategy = GREEDY_RATIO_DA_MVI;
        } else if (!strcmp(strategy_name, "greedy_price_ua_mvi")) {
          this->selected_strategy = GREEDY_PRICE_UA_MVI;
        } else if (!strcmp(strategy_name, "greedy_price_da_mvi")) {
          this->selected_strategy = GREEDY_PRICE_DA_MVI;
        } else if (!strcmp(strategy_name, "greedy_weight_ua_mvi")) {
          this->selected_strategy = GREEDY_WEIGHT_UA_MVI;
        } else if (!strcmp(strategy_name, "greedy_weight_da_mvi")) {
          this->selected_strategy = GREEDY_WEIGHT_DA_MVI;
        } else if (!strcmp(strategy_name, "dynamic_programming")) {
          this->selected_strategy = DYNAMIC_PROGRAMMING;
        } else if (!strcmp(strategy_name, "branch_and_bound")) {
          this->selected_strategy = BRANCH_AND_BOUND;
        } else if (!strcmp(strategy_name, "simulated_annealing")) {
          this->selected_strategy = SIMULATED_ANNEALING;
        } else if (!strcmp(strategy_name, "bruteforce")) {
          this->selected_strategy = BRUTE_FORCE;
        } else {
          fprintf(stderr, "Unknown strategy selected: '%s'\n", strategy_name);
          print_availible_strategies();
          /* default strategy */
          this->selected_strategy = GREEDY_RATIO_DA_MVI;
          fprintf(stderr, "Default strategy selected 'greedy_ratio_da_mvi'\n");
        }
      }

    void solve() {
      using namespace Algorithms;
      switch (this->selected_strategy) {
        case BRUTE_FORCE:
          solve_using_bruteforce();
          break;
        case GREEDY_RATIO_UA:
        case GREEDY_RATIO_DA:
        case GREEDY_PRICE_UA:
        case GREEDY_PRICE_DA:
        case GREEDY_WEIGHT_UA:
        case GREEDY_WEIGHT_DA:
        case GREEDY_RATIO_UA_MVI:
        case GREEDY_RATIO_DA_MVI:
        case GREEDY_PRICE_UA_MVI:
        case GREEDY_PRICE_DA_MVI:
        case GREEDY_WEIGHT_UA_MVI:
        case GREEDY_WEIGHT_DA_MVI:
          solve_using_greedy();
          break;
        case BRANCH_AND_BOUND:
          {
            Branch_and_Bound bab(this->knapsack, this->instance);
            bab.go();
            this->knapsack = bab.get_knapsack();
            this->instance = bab.get_instance();
          }
          break;
        case DYNAMIC_PROGRAMMING:
          {
            Dynamic_programming dp(this->knapsack, this->instance);
            dp.go();
            this->knapsack = dp.get_knapsack();
            this->instance = dp.get_instance();
          }
          break;
        case SIMULATED_ANNEALING:
          {
            Simulated_annealing sa(this->knapsack, this->instance);
            sa.go();
            this->knapsack = sa.get_knapsack();
            this->instance = sa.get_instance();
          }
          break;
        default:
          fprintf(stderr, "Unknown strategy selected\n");
          exit(1);
          break;
      }
    }

    void print_result() {
      printf("%i ", this->instance.get_id());
      printf("%i ", this->instance.get_items_count());
      printf("%i  ", this->knapsack.get_value());
      for (unsigned i = 0; i < this->instance.get_items_count(); i++) {
        printf("%i", static_cast<int>(this->instance.get_item(i).is_taken()));
        (i == (this->instance.get_items_count() - 1)) ?
          printf("\n") : printf(" ");
      }
      fprintf(stderr, "%lu\n", this->instance.get_number_of_tested_states());
    }
};

int main(const int argc, char **argv) {
  if (argc != 3) {
    printf("Usage: %s <algorithm> <input data>\n", *argv);
    return 0;
  }

  IOHandler iohandler;
  if (iohandler.load_data(argv[2]) != 0) {
    return 1;
  }

  srand(static_cast<unsigned>(time(NULL)));

  for (int i = 0; i < iohandler.instances_count(); i++) {
    Instance instance(iohandler.get_instance(i));
    Knapsack knapsack(instance.get_max_load());
    Solver solver(knapsack, instance, argv[1]);
    solver.solve();
    solver.print_result();
  }
  return 0;
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
